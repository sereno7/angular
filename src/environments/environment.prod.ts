export const environment = {
  AdopteUnDev_BASE_URL: 'http://localhost:50952/api/skill/',
  AdopteUnDev_ENTERPRISE: 'http://localhost:50952/api/enterprise/',
  AdopteUnDev_DEVELOPPER: 'http://localhost:50952/api/developper/',
  AdopteUnDev_ADVERT: 'http://localhost:50952/api/advert/',
  AdopteUnDev_MESSAGE: 'http://localhost:50952/api/message/',
  AdopteUnDev_USER: 'http://localhost:50952/api/user/',
  production: true
};
