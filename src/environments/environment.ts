// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  AdopteUnDev_BASE_URL: 'http://localhost:50952/api/skill/',
  AdopteUnDev_ENTERPRISE: 'http://localhost:50952/api/enterprise/',
  AdopteUnDev_DEVELOPPER: 'http://localhost:50952/api/developper/',
  AdopteUnDev_ADVERT: 'http://localhost:50952/api/advert/',
  AdopteUnDev_MESSAGE: 'http://localhost:50952/api/message/',
  AdopteUnDev_USER: 'http://localhost:50952/api/user/',
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
