import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  items: NbMenuItem[]
  currentUser: User;
  constructor(
        private router: Router,
        private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }
  ngOnInit(): void {
    this.items = [
      { title: 'Skill', link: '/skill', icon: 'book', children:[
        { title: 'Liste des Skills', link: '/skill', icon: 'book'},
        { title: 'Ajouter un Skill', link: '/addSkill', icon: 'plus'},
      ]},
      { title: 'Entreprise', link: '/enterprise', icon: 'book', children:[
        { title: 'Liste des Entreprises', link: '/enterprise', icon: 'book'},
        { title: 'Ajouter une entreprise', link: '/addEnterprise', icon: 'plus' }, 
      ]},
      { title: 'Developpeur', link: '/enterprise', icon: 'book', children:[
        { title: 'Liste des Developpeur', link: '/developper', icon: 'book'},
        { title: 'Ajouter un Developpeur', link: '/addDevelopper', icon: 'plus' }, 
      ]},
      { title: 'Annonces', link: '/advert', icon: 'book', children:[
        { title: 'Liste des Annonces', link: '/advert', icon: 'book'},
      ]},          
    ];
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
}
