import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillComponent } from './components/skill/skill.component';
import { AddSkillComponent } from './components/skill/add-skill/add-skill.component';
import { MessageComponent } from './components/message/message.component';
import { EnterpriseComponent } from './components/enterprise/enterprise.component';
import { AddEnterpriseComponent } from './components/enterprise/add-enterprise/add-enterprise.component';
import { EditSkillComponent } from './components/skill/edit-skill/edit-skill.component';
import { EditEnterpriseComponent } from './components/enterprise/edit-enterprise/edit-enterprise.component';
import { SkillResolver } from './resolvers/resolver';
import { EnterpriseResolver } from './resolvers/enterprise';
import { DevelopperComponent } from './components/developper/developper.component';
import { AddDevelopperComponent } from './components/developper/add-developper/add-developper.component';
import { DevelopperResolver } from './resolvers/developper';
import { EditDevelopperComponent } from './components/developper/edit-developper/edit-developper.component';
import { AdvertComponent } from './components/Advert/advert.component';
import { AdvertByIdEnterpriseComponent } from './components/Advert/advert-by-id-enterprise/advert-by-id-enterprise.component';
import { AdvertResolver } from './resolvers/advert';
import { AddAdvertComponent } from './components/Advert/advert-by-id-enterprise/add-advert/add-advert.component';
import { EditAdvertComponent } from './components/Advert/advert-by-id-enterprise/edit-advert/edit-advert.component';
import { EditAdvertResolver } from './resolvers/editAdvert';
import { EditDevelopperResolver } from './resolvers/editDevelopper';
import { AdvertByDevSkillComponent } from './components/Advert/advert-by-dev-skill/advert-by-dev-skill.component';
import { AdvertLikedByComponent } from './components/Advert/advert-liked-by/advert-liked-by.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './helper/auth.guard';


const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  // otherwise redirect to home
  // { path: '**', redirectTo: '' },
  { path: 'skill', component: SkillComponent },
  { path: 'addSkill', component: AddSkillComponent, canActivate: [AuthGuard] },
  { 
    path: 'editSkill/:id', 
    component: EditSkillComponent,
    resolve: { resolveSkill: SkillResolver } 
  },


  { path: 'enterprise', component: EnterpriseComponent },
  { path: 'addEnterprise', component: AddEnterpriseComponent },
  { 
    path: 'editEnterprise/:id', 
    component: EditEnterpriseComponent,
    resolve: { resolveEnterprise: EnterpriseResolver }
   },


  { path: 'developper', component: DevelopperComponent },  
  { path: 'addDevelopper', component: AddDevelopperComponent },
   { 
    path: 'editDevelopper/:id', 
    component: EditDevelopperComponent,
    resolve: { resolveEditDevelopper: EditDevelopperResolver }
   },


  { path: 'advert', component: AdvertComponent },  
  { 
    path: 'advertByIdEnterprise/:id', 
    component: AdvertByIdEnterpriseComponent,
    resolve: { resolveEnterprise: EnterpriseResolver}
   },
  { path: 'addAdvert/:id', component: AddAdvertComponent },
  { 
    path: 'editAdvert/:id', 
    component: EditAdvertComponent,
    resolve: { resolveEditAdvert: EditAdvertResolver }
  },
  { 
    path: 'advertByDevSkill/:id', 
    component: AdvertByDevSkillComponent,
    resolve: { resolveDevelopper: DevelopperResolver}
   },
   { 
    path: 'advertLikedBy/:id', 
    component: AdvertLikedByComponent,
    resolve: { resolveAdvert: EditAdvertResolver}
   },


  { 
    path: 'message/:idD/:idA', 
    component: MessageComponent,
    // resolve: { resolveAdvert: EditAdvertResolver}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
