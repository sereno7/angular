import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbMenuModule, NbButtonModule, NbDialogModule, NbCardModule, NbListModule, NbToastrModule, NbInputModule, NbIconModule, NbTreeGridModule, NbCheckboxModule, NbChatModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ReactiveFormsModule } from '@angular/forms';
import { SkillComponent } from './components/skill/skill.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { AddSkillComponent } from './components/skill/add-skill/add-skill.component';
import { MessageComponent } from './components/message/message.component';
import { EnterpriseComponent } from './components/enterprise/enterprise.component';
import { AddEnterpriseComponent } from './components/enterprise/add-enterprise/add-enterprise.component';
import { EditSkillComponent } from './components/skill/edit-skill/edit-skill.component';
import { EditEnterpriseComponent } from './components/enterprise/edit-enterprise/edit-enterprise.component';
import { DevelopperComponent } from './components/developper/developper.component';
import { AddDevelopperComponent } from './components/developper/add-developper/add-developper.component';
import { EditDevelopperComponent } from './components/developper/edit-developper/edit-developper.component';
import { AdvertComponent } from './components/Advert/advert.component';
import { MatTableModule } from '@angular/material/table';
import { AdvertByIdEnterpriseComponent } from './components/Advert/advert-by-id-enterprise/advert-by-id-enterprise.component';
import { AddAdvertComponent } from './components/Advert/advert-by-id-enterprise/add-advert/add-advert.component';
import { EditAdvertComponent } from './components/Advert/advert-by-id-enterprise/edit-advert/edit-advert.component';
import { AdvertByDevSkillComponent } from './components/Advert/advert-by-dev-skill/advert-by-dev-skill.component';
import { AdvertLikedByComponent } from './components/Advert/advert-liked-by/advert-liked-by.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import {JwtInterceptor} from '../app/interceptors/jwt.interceptor';
import {ErrorInterceptor} from '../app/interceptors/error.interceptor'




@NgModule({
  declarations: [
    AppComponent,
    SkillComponent,
    LoaderComponent,
    AddSkillComponent,
    MessageComponent,
    EnterpriseComponent,
    AddEnterpriseComponent,
    EditSkillComponent,
    EditEnterpriseComponent,
    DevelopperComponent,
    AddDevelopperComponent,
    EditDevelopperComponent,
    AdvertComponent,
    AdvertByIdEnterpriseComponent,
    AddAdvertComponent,
    EditAdvertComponent,
    AdvertByDevSkillComponent,
    AdvertLikedByComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent

    

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbButtonModule,
    NbDialogModule.forRoot(),
    NbCardModule,
    NbListModule,
    HttpClientModule,
    ReactiveFormsModule,
    NbInputModule,
    NbToastrModule.forRoot(),
    NbIconModule,
    MatTableModule,
    NbCheckboxModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
