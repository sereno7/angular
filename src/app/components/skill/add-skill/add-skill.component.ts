import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { SkillService } from '../../../services/skill.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-skill',
  templateUrl: './add-skill.component.html',
  styleUrls: ['./add-skill.component.scss']
})
export class AddSkillComponent implements OnInit {
  fg: FormGroup;
  constructor(
    private SkillService: SkillService,
    private toastr: NbToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
    });
  }
  submit() {
    if(this.fg.valid) {
      this.SkillService.add(this.fg.value)
        .subscribe(data => {
          this.toastr.success('cool');
          this.router.navigateByUrl('/skill')
        }, error => {
          this.toastr.danger('zut');
        });
    }
  }

}
