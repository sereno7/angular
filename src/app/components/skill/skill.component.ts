import { Component, OnInit } from '@angular/core';
import { SkillService } from '../../services/skill.service';
import { Skill } from '../../models/skill';

@Component({
  selector: 'app-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit {

  model: Skill[];
  constructor(private SkillService: SkillService) { }

  ngOnInit(): void {
    this.SkillService.context.subscribe(data => {
      this.model = data;
    })
    this.SkillService.refresh();
  }

    delete(id: number){
      this.SkillService.delete(id).subscribe();
    }
}

