import { Component, OnInit } from '@angular/core';
import { Skill } from '../../../models/skill';
import { SkillService } from '../../../services/skill.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-edit-skill',
  templateUrl: './edit-skill.component.html',
  styleUrls: ['./edit-skill.component.scss']
})
export class EditSkillComponent implements OnInit {
  fg: FormGroup;
  constructor(
    private SkillService: SkillService,
    private toastr: NbToastrService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    let data = this.route.snapshot.data.resolveSkill;

        //this.model = data;
        this.fg = new FormGroup({
          'Id': new FormControl(data.Id)
          ,
          'Name': new FormControl(data.Name, Validators.compose([
            Validators.required,
            Validators.maxLength(50)
          ])),
        });

    
    
  }

  edit() {
    if(this.fg.valid) {
      this.SkillService.edit(this.fg.value)
        .subscribe(data => {
          this.toastr.success("Le skill à été modifié");
          this.router.navigateByUrl('/skill')
        }, error => {
          this.toastr.danger("Une erreur s'est produite");
        });
    }
  }
}
