import { Component, OnInit } from '@angular/core';
import { Message } from '../../models/message';
import { ActivatedRoute } from '@angular/router';
import  {MessageService} from '../../services/message.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { AuthenticationService } from '../../services/authentication.service';



@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
  
})
export class MessageComponent implements OnInit {
  fg: FormGroup;
  displayedColumns: string[] = ['Date', 'Name', 'Subject', 'Supprimer'];
  dataSource: Message[];
  idD: number;
  idA: number;
  lastNameUser: string;
  firstNameUser: string;



  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private toastr: NbToastrService,
    private authService: AuthenticationService,

  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({});
    this.idA = this.route.snapshot.params.idA;
    this.idD = this.route.snapshot.params.idD;
    this.authService.currentUser.subscribe(data=>{
      this.lastNameUser = data.LastName;
      this.firstNameUser = data.FirstName;
    })
    this.messageService.GetMessageExchange(this.idA, this.idD).subscribe(data =>{
      this.dataSource = data;
    });
  

    this.fg = new FormGroup({
      'Date': new FormControl(new Date())
      ,
      'Subject': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(1024)
      ])),
      'Name': new FormControl(this.lastNameUser),
      'IdAdvert':new FormControl( this.idA),
      'IdDevelopper':new FormControl( this.idD),    
    });  
  }

  submit() {
    if(this.fg.valid) {
      this.messageService.add(this.fg.value)
        .subscribe(data => {
          this.messageService.GetMessageExchange(this.idA, this.idD).subscribe(data =>{
            this.dataSource = data;
          });
          this.toastr.success('cool');
        }, error => {
          this.toastr.danger('zut');
        });
    }
  }
  delete(id: number){
    this.messageService.delete(id).subscribe(data => {
      this.messageService.GetMessageExchange(this.idA, this.idD).subscribe(data =>{
        this.dataSource = data;
      });
    })
  }



  // sendMessage(event: any) {
  //   const files = !event.files ? [] : event.files.map((file) => {
  //     return {
  //       url: file.src,
  //       type: file.type,
  //       icon: 'file-text-outline',
  //     };
  //   });

  //   this.dataSource.push({
  //     text: event.message,
  //     date: new Date(),
  //     reply: true,
  //     type: files.length ? 'file' : 'text',
  //     files: files,
  //     user: {
  //       name: 'Jonh Doe',
  //       avatar: 'https://i.gifer.com/no.gif',
  //     },
  //   });
  //   const botReply = this.messageService.reply(event.message);
  //   if (botReply) {
  //     setTimeout(() => { this.dataSource.push(botReply) }, 500);
  //   }
  // }
}
