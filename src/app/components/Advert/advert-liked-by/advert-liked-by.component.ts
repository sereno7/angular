import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvertService } from '../../../services/advert.service';
import { Advert } from '../../../models/advert';
import { Developper } from '../../../models/developper';
import { AdvertDev } from '../../../models/advertDev';

@Component({
  selector: 'app-advert-liked-by',
  templateUrl: './advert-liked-by.component.html',
  styleUrls: ['./advert-liked-by.component.scss']
})
export class AdvertLikedByComponent implements OnInit {
  model: Advert;
  displayedColumns: string[] = ['Last_Name', 'First_Name', 'Email', 'Like', 'Inbox'];
  dataSource: Developper[];
  idS: number[];

  constructor(
    private route: ActivatedRoute,
    private advertService: AdvertService,
  ) { }

  ngOnInit(): void {
    this.model = this.route.snapshot.data.resolveAdvert;
    this.advertService.GetDevByLikedAdvert(this.model.Id).subscribe(data =>{
      this.dataSource = data;
    })
    this.advertService.getLikedByAdvertStatus(this.model.Id).subscribe(data =>{     
      this.idS = data.filter(x => x.AdvertAcceptDev == true).map(x=>x.IdDevelopper);
    });
  }

  liked(isChecked: boolean, idD: number) {
    if(isChecked)
    {
    let advertDevModel: AdvertDev = { IdDevelopper: idD, IdAdvert: this.model.Id, AdvertAcceptDev : true, DevAcceptAdvert: true };
    this.advertService.AddLikeToDev(advertDevModel).subscribe(()=>{
      this.advertService.getLikedByAdvertStatus(this.model.Id).subscribe(data =>{     
        this.idS = data.filter(x => x.AdvertAcceptDev == true).map(x=>x.IdDevelopper);
      });
    });
    }
    else
    {
      let advertDevModel: AdvertDev = { IdDevelopper: idD, IdAdvert: this.model.Id, AdvertAcceptDev: false, DevAcceptAdvert: true };
      this.advertService.RemoveLikeFromDev(advertDevModel).subscribe(()=>{
        this.advertService.getLikedByAdvertStatus(this.model.Id).subscribe(data =>{     
          this.idS = data.filter(x => x.AdvertAcceptDev == true).map(x=>x.IdDevelopper);
        });
      });
    }
  }

}
