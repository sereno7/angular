import { Component, OnInit, Input } from '@angular/core';
import{ Advert } from '../../models/advert'
import { AdvertService } from '../../services/advert.service';
@Component({
  selector: 'app-advert',
  templateUrl: './advert.component.html',
  styleUrls: ['./advert.component.scss']
})
export class AdvertComponent implements OnInit {
  model: Advert[]
  displayedColumns: string[] = ['Date', 'Name', 'Description', 'Developpeur'];
  dataSource = this.model;
  constructor(private advertService: AdvertService) { }

  ngOnInit(): void {
    this.advertService.context.subscribe(data =>{
      this.model = data;
    })
  }
}
