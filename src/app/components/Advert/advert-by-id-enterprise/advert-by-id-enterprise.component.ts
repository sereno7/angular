import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdvertService } from '../../../services/advert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { NbToastrService } from '@nebular/theme';
import { Advert } from '../../../models/advert';
import { Enterprise } from '../../../models/enterprise';
import { EnterpriseService } from '../../../services/enterprise.service';

@Component({
  selector: 'app-advert-by-id-enterprise',
  templateUrl: './advert-by-id-enterprise.component.html',
  styleUrls: ['./advert-by-id-enterprise.component.scss']
})
export class AdvertByIdEnterpriseComponent implements OnInit {
  model: Enterprise;
  displayedColumns: string[] = ['Date', 'Name', 'Description', 'Supprimer', 'Modifier', 'LikedByDev'];
  dataSource: Advert[];

  constructor(
    private advertService: AdvertService,
    private EnterpriseService: EnterpriseService,
    private route: ActivatedRoute,
    private router: Router,
    
  ) { }

  ngOnInit(): void {
    this.model = this.route.snapshot.data.resolveEnterprise;
    this.dataSource = this.model.Adverts;   
  }

  delete(id: number): void{
      this.advertService.delete(id).subscribe(data => {
      //mise a jour du datasource
      this.EnterpriseService.getById(this.model.Id).subscribe(x =>
        {
          this.dataSource=x.Adverts;
        })
        //Autre methode datasource MAJ locale
      // this.dataSource = this.dataSource.filter(x => x.Id != id)
    })
  }
}

