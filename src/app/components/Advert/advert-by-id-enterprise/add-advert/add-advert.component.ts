import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
import { AdvertService } from '../../../../services/advert.service';
import { SkillService } from '../../../../services/skill.service';
import { Skill } from '../../../../models/skill';
import { AdvertSkill } from '../../../../models/advertSkill';


@Component({
  selector: 'app-add-advert',
  templateUrl: './add-advert.component.html',
  styleUrls: ['./add-advert.component.scss']
})
export class AddAdvertComponent implements OnInit {
  fg: FormGroup;
  idA: number;
  listeSkill: Skill[];
  isCreated: boolean = false;
  checked = false;
  

  constructor(
    private advertService: AdvertService,
    private skillService: SkillService,
    private toastr: NbToastrService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {

    this.fg = new FormGroup({
      'Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Date': new FormControl(new Date())
      ,
      'Description': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(1024)
      ])),
      'IdEnterprise':new FormControl( this.route.snapshot.params.id)
    });
  }
  submit() {
    if(this.fg.valid) {
      this.advertService.add(this.fg.value)
        .subscribe(data => {
          this.idA = data;
          this.toastr.success('cool');
          this.isCreated = true;

          // recuperer les skills
          this.skillService.context.subscribe(data => {
            this.listeSkill = data;
        }, error => {
          this.toastr.danger('zut');
        });
      })
    }    
  }
  addSkillToAdvert(isChecked: boolean, idSkill: number) {
    if(isChecked)
    {
    let advertSkillModel: AdvertSkill = { IdSkill: idSkill, IdAdvert: this.idA };
    this.advertService.addSkillToAdvert(advertSkillModel).subscribe();
    }
    else
    {
      let advertSkillModel: AdvertSkill = { IdSkill: idSkill, IdAdvert: this.idA };
      this.advertService.removeSkillFromAdvert(advertSkillModel).subscribe();
    }
  }
}
