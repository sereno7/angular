import { Component, OnInit } from '@angular/core';
import { Advert } from '../../../../models/advert';
import { AdvertService } from '../../../../services/advert.service';
import { SkillService } from '../../../../services/skill.service';
import { NbToastrService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Skill } from '../../../../models/skill';
import { AdvertSkill } from '../../../../models/advertSkill';

@Component({
  selector: 'app-edit-advert',
  templateUrl: './edit-advert.component.html',
  styleUrls: ['./edit-advert.component.scss']
})
export class EditAdvertComponent implements OnInit {
  fg: FormGroup;
  listeSkill: Skill[];
  listeIds: number[];
  idA: number;
  constructor(
    private advertService: AdvertService,
    private toastr: NbToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private skillService: SkillService
    ) {}

  ngOnInit(): void {   
    let data : Advert = this.route.snapshot.data.resolveEditAdvert;
    this.idA = data.Id;
    this.advertService.getAdvertSkill(this.idA).subscribe(n => {
      this.listeIds = n;
    });
    this.skillService.context.subscribe(data => {
      this.listeSkill = data;})
    this.fg = new FormGroup({
      'Name': new FormControl(data.Name, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Date': new FormControl(new Date())
      ,
      'Description': new FormControl(data.Description,Validators.compose([
        Validators.required,
        Validators.maxLength(1024)
      ])),
      'IdEnterprise':new FormControl(data.IdEnterprise),
      'Id':new FormControl(data.Id),

    });
  }
  submit() {
    if(this.fg.valid) {
      this.advertService.edit(this.fg.value)
      .subscribe(data => {
        this.toastr.success("L'annonce à été modifié");
      }, error => {
        this.toastr.danger("Une erreur s'est produite");
      });
    }
  }

  addSkillToAdvert(isChecked: boolean, idSkill: number) {
    if(isChecked)
    {
    let advertSkillModel: AdvertSkill = { IdSkill: idSkill, IdAdvert: this.idA };
    this.advertService.addSkillToAdvert(advertSkillModel).subscribe();
    }
    else
    {
      let advertSkillModel: AdvertSkill = { IdSkill: idSkill, IdAdvert: this.idA };
      this.advertService.removeSkillFromAdvert(advertSkillModel).subscribe();
    }
  }
}
