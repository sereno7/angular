import { Component, OnInit } from '@angular/core';
import { Developper } from '../../../models/developper';
import { Advert } from '../../../models/advert';
import { ActivatedRoute } from '@angular/router';
import { AdvertDev } from '../../../models/advertDev';
import { AdvertService } from '../../../services/advert.service';
import { DevelopperService } from '../../../services/developper.service';


@Component({
  selector: 'app-advert-by-dev-skill',
  templateUrl: './advert-by-dev-skill.component.html',
  styleUrls: ['./advert-by-dev-skill.component.scss']
})
export class AdvertByDevSkillComponent implements OnInit {
  model: Developper;
  displayedColumns: string[] = ['Date', 'Name', 'Description', 'Like', 'Inbox'];
  dataSource: Advert[] = [];
  idDevAdvert: AdvertDev[]= [];
  idS: number[]= [];
  idMatch: number[]= [];
  
  
  constructor(
    private route: ActivatedRoute,
    private advertService: AdvertService,
    private developperService: DevelopperService,
  ) { }

  ngOnInit(): void {
    this.model = this.route.snapshot.data.resolveDevelopper;
    this.dataSource = this.model.Adverts;

    this.advertService.getLikedStatus(this.model.Id).subscribe(data =>{
      this.idS = data.map(x=>x.IdAdvert);
    });

    this.advertService.getLikedStatus(this.model.Id).subscribe(data =>{
      this.idMatch = data.filter(x => x.AdvertAcceptDev == true).map(x=>x.IdAdvert);
    });
  }

  liked(isChecked: boolean, idA: number) {
    if(isChecked)
    {
    let advertDevModel: AdvertDev = { IdDevelopper: this.model.Id, IdAdvert: idA, AdvertAcceptDev : false, DevAcceptAdvert: true };
    this.advertService.addLikeToAdvert(advertDevModel).subscribe(()=>{
      this.advertService.getLikedStatus(this.model.Id).subscribe(data =>{
        this.idMatch = data.filter(x => x.AdvertAcceptDev == true).map(x=>x.IdAdvert);
      });
    });
    // this.developperService.getById(this.model.Id).subscribe(x =>
    //   {
    //     this.dataSource=x.Adverts;
    //   })
    }
    else
    {
      let advertDevModel: AdvertDev = { IdDevelopper: this.model.Id, IdAdvert: idA, AdvertAcceptDev: false, DevAcceptAdvert: false };
      this.advertService.removeLikeFromAdvert(advertDevModel).subscribe(()=>{
        this.advertService.getLikedStatus(this.model.Id).subscribe(data =>{
          this.idMatch = data.filter(x => x.AdvertAcceptDev == true).map(x=>x.IdAdvert);
        });
      });
      // this.developperService.getById(this.model.Id).subscribe(x =>
      //   {
      //     this.dataSource=x.Adverts;
      //   })
    }
  }

}
