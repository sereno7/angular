import { Component, OnInit } from '@angular/core';
import { Developper } from '../../models/developper';
import { DevelopperService } from '../../services/developper.service';

@Component({
  selector: 'app-developper',
  templateUrl: './developper.component.html',
  styleUrls: ['./developper.component.scss']
})
export class DevelopperComponent implements OnInit {
  model: Developper[]
  constructor(private developperService: DevelopperService) { }

  ngOnInit(): void {
    this.developperService.context.subscribe(data =>{
      this.model = data;
    })
  }

  delete(id: number){
    this.developperService.delete(id).subscribe();
  }
}
