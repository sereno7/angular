import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DevelopperService } from '../../../services/developper.service';
import { NbToastrService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Skill } from '../../../models/skill';
import { SkillService } from '../../../services/skill.service';
import { DevelopperSkill } from '../../../models/DevelopperSkill';
import { Developper } from '../../../models/developper';


@Component({
  selector: 'app-edit-developper',
  templateUrl: './edit-developper.component.html',
  styleUrls: ['./edit-developper.component.scss'],
  providers: [ DatePipe ]
})
export class EditDevelopperComponent implements OnInit {
  fg: FormGroup;
  listeSkill: Skill[];
  listeIds: number[];
  idD: number;
  constructor(
    private developperService: DevelopperService,
    private toastr: NbToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private skillService: SkillService
  ) { }

  ngOnInit(): void {
    let data: Developper= this.route.snapshot.data.resolveEditDevelopper;
    this.idD = data.Id;
    this.developperService.getDevSkill(this.idD).subscribe(n => {
      this.listeIds = n;
    });
    this.skillService.context.subscribe(data => {
      this.listeSkill = data;})
    this.fg = new FormGroup({
      'Id': new FormControl(data.Id)
      ,
      'Last_Name': new FormControl(data.Last_Name, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'First_Name': new FormControl(data.First_Name,Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Email': new FormControl(data.Email,Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])),
      'Adresse': new FormControl(data.Adresse,Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])),
      'Tel': new FormControl(data.Tel,Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern("^[0-9]*$"),
      ])),
      'Birth_date': new FormControl(this.datePipe.transform(data.Birth_date, 'yyyy-MM-dd'),Validators.compose([
        // Validators.required,
        Validators.maxLength(25),
      ])),
    });
  }
  
  edit() {
    if(this.fg.valid) {
      this.developperService.edit(this.fg.value)
        .subscribe(data => {
          this.toastr.success("Le compte dev à été modifié");
          // this.router.navigateByUrl('/developper')
        }, error => {
          this.toastr.danger("Une erreur s'est produite");
        });
    }
  }
  addSkillToDev(isChecked: boolean, idSkill: number) {
    if(isChecked)
    {
    let advertSkillModel: DevelopperSkill = { IdSkill: idSkill, IdDevelopper: this.idD };
    this.developperService.addSkillToDev(advertSkillModel).subscribe();
    }
    else
    {
      let advertSkillModel: DevelopperSkill = { IdSkill: idSkill, IdDevelopper: this.idD };
      this.developperService.removeSkillFromDev(advertSkillModel).subscribe();
    }
  }
}
