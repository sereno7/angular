import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { DevelopperService } from '../../../services/developper.service';
import { Skill } from '../../../models/skill';
import { DevelopperSkill } from '../../../models/DevelopperSkill';
import { SkillService } from '../../../services/skill.service';

@Component({
  selector: 'app-add-developper',
  templateUrl: './add-developper.component.html',
  styleUrls: ['./add-developper.component.scss']
})
export class AddDevelopperComponent implements OnInit {
  fg: FormGroup;
  isCreated: boolean = false;
  listeSkill: Skill[];
  idD: number;
  checked = false;
  constructor(
    private DevelopperService: DevelopperService,
    private toastr: NbToastrService,
    private skillService: SkillService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.fg = new FormGroup({
      'Last_Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'First_Name': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Email': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])),
      'Adresse': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])),
      'Tel': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern("^[0-9]*$"),
      ])),
      'Birth_date': new FormControl(null,Validators.compose([
        // Validators.required,
        Validators.maxLength(25),
      ])),
    });
  }

  submit() {
    if(this.fg.valid) {
      this.DevelopperService.add(this.fg.value)
        .subscribe(data => {
          this.idD = data;
          this.toastr.success('cool');
          this.isCreated = true;
          this.skillService.context.subscribe(data => {
            this.listeSkill = data;
          // this.router.navigateByUrl('/developper')
        }, error => {
          this.toastr.danger('zut');
        });
    })
  }
}

  toggleSkillDev(isChecked: boolean, idSkill: number) {
    if(isChecked)
    {
    let DevSkillModel: DevelopperSkill = { IdSkill: idSkill, IdDevelopper: this.idD };
    this.DevelopperService.addSkillToDev(DevSkillModel).subscribe();
    }
    else
    {
      let advertSkillModel: DevelopperSkill = { IdSkill: idSkill, IdDevelopper: this.idD };
      this.DevelopperService.removeSkillFromDev(advertSkillModel).subscribe();
    }
  }
}
