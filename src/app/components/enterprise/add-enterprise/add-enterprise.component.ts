import { Component, OnInit } from '@angular/core';
import { Enterprise } from '../../../models/enterprise';
import { EnterpriseService } from '../../../services/enterprise.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-enterprise',
  templateUrl: './add-enterprise.component.html',
  styleUrls: ['./add-enterprise.component.scss']
})
export class AddEnterpriseComponent implements OnInit {
  fg: FormGroup;
  
  constructor(
    private EnterpriseService: EnterpriseService,
    private toastr: NbToastrService,
    private router: Router) { }

  ngOnInit(): void {
    
    this.fg = new FormGroup({
      'Name': new FormControl(null, Validators.compose([
        Validators.required,
        Validators.maxLength(50)
      ])),
      'Adresse': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(255)
      ])),
      'Email': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(100)
      ])),
      'Tel': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern("^[0-9]*$"),
      ])),
      'Fax': new FormControl(null,Validators.compose([
        // Validators.required,
        Validators.maxLength(9),
        Validators.pattern("^[0-9]*$"),
      ])),
      'Tva': new FormControl(null,Validators.compose([
        Validators.required,
        Validators.maxLength(20)
      ])),
    });
  }

  submit() {
    if(this.fg.valid) {
      this.EnterpriseService.add(this.fg.value)
        .subscribe(data => {
          this.toastr.success('cool');
          this.router.navigateByUrl('/enterprise')
        }, error => {
          this.toastr.danger('zut');
        });
    }
  }
}
