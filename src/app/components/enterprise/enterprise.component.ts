import { Component, OnInit } from '@angular/core';
import { Enterprise } from '../../models/enterprise';
import { EnterpriseService } from '../../services/enterprise.service';

@Component({
  selector: 'app-enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.scss']
})
export class EnterpriseComponent implements OnInit {
  model: Enterprise[]
  constructor(private EnterpriseService: EnterpriseService) { }

  ngOnInit(): void {
    this.EnterpriseService.context.subscribe(data =>{
      this.model = data;
    })
  }

  delete(id: number){
    this.EnterpriseService.delete(id).subscribe();
  }
}
