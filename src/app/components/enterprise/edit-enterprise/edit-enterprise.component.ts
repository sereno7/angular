import { Component, OnInit } from '@angular/core';
import { EnterpriseService } from '../../../services/enterprise.service';
import { NbToastrService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-enterprise',
  templateUrl: './edit-enterprise.component.html',
  styleUrls: ['./edit-enterprise.component.scss']
})
export class EditEnterpriseComponent implements OnInit {
  fg: FormGroup;
  constructor(
    private EnterpriseService: EnterpriseService,
    private toastr: NbToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {

    let data = this.route.snapshot.data.resolveEnterprise;

        this.fg = new FormGroup({
          'Id': new FormControl(data.Id)
          ,
          'Name': new FormControl(data.Name, Validators.compose([
            Validators.required,
            Validators.maxLength(50)
          ])),
          'Adresse': new FormControl(data.Adresse,Validators.compose([
            Validators.required,
            Validators.maxLength(255)
          ])),
          'Email': new FormControl(data.Email,Validators.compose([
            Validators.required,
            Validators.maxLength(100)
          ])),
          'Tel': new FormControl(data.Tel,Validators.compose([
            Validators.required,
            Validators.maxLength(10),
            Validators.pattern("^[0-9]*$"),
          ])),
          'Fax': new FormControl(data.Fax,Validators.compose([
            // Validators.required,
            Validators.maxLength(9),
            Validators.pattern("^[0-9]*$"),
          ])),
          'Tva': new FormControl(data.Tva,Validators.compose([
            Validators.required,
            Validators.maxLength(20)
          ])),
        })
    }

    edit() {
      if(this.fg.valid) {
        this.EnterpriseService.edit(this.fg.value)
          .subscribe(data => {
            this.toastr.success("L'entreprise à été modifié");
            this.router.navigateByUrl('/enterprise')
          }, error => {
            this.toastr.danger("Une erreur s'est produite");
          });
      }
    }
  }
  
