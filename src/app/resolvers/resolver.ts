import { Resolve, ActivatedRoute } from "@angular/router";
import { Injectable } from "@angular/core";
import { Skill } from "../models/skill";
import { SkillService } from "../services/skill.service";


@Injectable({
    providedIn: 'root'
  })
  
export class SkillResolver implements Resolve<Skill>{
    constructor(private skillService:SkillService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Skill | import("rxjs").Observable<Skill> | Promise<Skill> {
        return this.skillService.getById(route.params.id);       
    }  
}