import { Resolve, ActivatedRoute } from "@angular/router";
import { Injectable } from "@angular/core";
import { Advert } from "../models/advert";
import { AdvertService } from "../services/advert.service";



@Injectable({
    providedIn: 'root'
  })
  
export class AdvertResolver implements Resolve<Advert[]>{
    constructor(private advertService:AdvertService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Advert[] | import("rxjs").Observable<Advert[]> | Promise<Advert[]> {
        return this.advertService.getByIdEnterprise(route.params.id);       
    }  
}