import { Resolve, ActivatedRoute } from "@angular/router";
import { Injectable } from "@angular/core";
import { Enterprise } from "../models/enterprise";
import { EnterpriseService } from "../services/enterprise.service";


@Injectable({
    providedIn: 'root'
  })
  
export class EnterpriseResolver implements Resolve<Enterprise>{
    constructor(private entepriseService:EnterpriseService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Enterprise | import("rxjs").Observable<Enterprise> | Promise<Enterprise> {
        return this.entepriseService.getById(route.params.id);       
    }  
}