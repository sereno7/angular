import { Resolve, ActivatedRoute } from "@angular/router";
import { Injectable } from "@angular/core";
import { Enterprise } from "../models/enterprise";
import { EnterpriseService } from "../services/enterprise.service";
import { Developper } from "../models/developper";
import { DevelopperService } from "../services/developper.service";


@Injectable({
    providedIn: 'root'
  })
  
export class DevelopperResolver implements Resolve<Developper>{
    constructor(private developperService:DevelopperService){}
    resolve(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot)
    : Developper | import("rxjs").Observable<Developper> | Promise<Developper> {
        return this.developperService.getById(route.params.id);       
    }  
}