import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import {Message} from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  // context: BehaviorSubject<Message[]>
  private URL: string = environment.AdopteUnDev_MESSAGE;
  constructor(private http: HttpClient) { 
    // this.context = new BehaviorSubject<Message[]>(null);
    // this.refresh();
  }

  // refresh() {
  //   this.http.get<Message[]>(this.URL).subscribe(data => {
  //     this.context.next(data);
  //   });
  // }

  GetMessageExchange(idA: number, idD: number) {
    return this.http.get<Message[]>(this.URL+ 'GetMessageExchange/'+ idA +'/' + idD);
  }
  
  add(model : Message) {
    return this.http.post<Message>(this.URL, model);
  }
  
  delete(id : number) {
    return this.http.delete(this.URL + id);
  }
}