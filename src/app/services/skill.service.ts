import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { BehaviorSubject } from 'rxjs';
import { Skill } from '../models/skill';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  context: BehaviorSubject<Skill[]>;
  private URL: string = environment.AdopteUnDev_BASE_URL;

  constructor(private http: HttpClient) { 
    this.context = new BehaviorSubject<Skill[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<Skill[]>(this.URL).subscribe(data => {
      this.context.next(data);
    });
  }
  add(model : Skill) {
    return this.http.post(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  delete(id : number) {
    return this.http.delete(this.URL + id).pipe(finalize(() => {
      this.refresh();
    }));
  }

  getById(id: number){
    return this.http.get<Skill>(this.URL + id);
  }

  edit(model: Skill) {
    return this.http.put(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  
}
