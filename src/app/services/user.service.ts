import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import {environment} from '../../environments/environment'

@Injectable({ providedIn: 'root' })
export class UserService {
    private URL: string = environment.AdopteUnDev_USER;
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(this.URL);
    }

    register(user: User) {
        console.log(user);
        console.log(this.URL)
        return this.http.post(this.URL, user);
    }

    delete(id: number) {
        return this.http.delete(this.URL + id);
    }
}