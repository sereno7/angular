import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import { Developper } from '../models/developper';
import { finalize } from 'rxjs/operators';
import { DevelopperSkill } from '../models/DevelopperSkill';

@Injectable({
  providedIn: 'root'
})
export class DevelopperService {
  
  context: BehaviorSubject<Developper[]>;
  private URL: string = environment.AdopteUnDev_DEVELOPPER;
  constructor(private http: HttpClient) {
    this.context = new BehaviorSubject<Developper[]>(null);
    this.refresh();
   }



refresh() {
  this.http.get<Developper[]>(this.URL).subscribe(data => {
    this.context.next(data);
  });
}
add(model : Developper) {
  return this.http.post<number>(this.URL, model).pipe(finalize(() => {
    this.refresh();
  }));
}

delete(id : number) {
  return this.http.delete(this.URL + id).pipe(finalize(() => {
    this.refresh();
  }));
}

getById(id: number){
  return this.http.get<Developper>(this.URL + id);
}


edit(model: Developper) {
  return this.http.put(this.URL, model).pipe(finalize(() => {
    this.refresh();
  }));
}



addSkillToDev(model : DevelopperSkill) {
  return this.http.post(this.URL+ "AddSkillToDevelopper/", model).pipe(finalize(() => { 
    this.refresh();
  }));
}

removeSkillFromDev(model : DevelopperSkill) {
  return this.http.post(this.URL+ "RemoveSkillFromDevelopper/", model).pipe(finalize(() => {   
    this.refresh();
  }));
}

getDevSkill(id: number){
  return this.http.get<number[]>(this.URL + "getDevelopperSkill/" + id);
}
}