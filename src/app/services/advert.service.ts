import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Advert } from '../models/advert';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { AdvertSkill } from '../models/advertSkill';
import { AdvertDev } from '../models/advertDev';
import { Developper } from '../models/developper';

@Injectable({
  providedIn: 'root'
})
export class AdvertService {
  context: BehaviorSubject<Advert[]>
  private URL: string = environment.AdopteUnDev_ADVERT;
  constructor(private http: HttpClient) { 
    this.context = new BehaviorSubject<Advert[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<Advert[]>(this.URL).subscribe(data => {
      this.context.next(data);
    });
  }
  
  add(model : Advert) {
    return this.http.post<number>(this.URL, model).pipe(finalize(() => { 
      this.refresh();
    }));
  }
  
  delete(id : number) {
    return this.http.delete(this.URL + id).pipe(finalize(() => {
      this.refresh();
    }));
  }
  
 
  getById(id: number){
    return this.http.get<Advert>(this.URL + id);
  }

  getByIdEnterprise(id: number){
    return this.http.get<Advert[]>(this.URL + "GetByIdEnterprise/" + id); 
  }

  addSkillToAdvert(model : AdvertSkill) {
    return this.http.post(this.URL+ "AddSkillToAdvert/", model).pipe(finalize(() => { 
      this.refresh();
    }));
  }

  removeSkillFromAdvert(model : AdvertSkill) {
    return this.http.post(this.URL+ "removeSkillFromAdvert/", model).pipe(finalize(() => {   
      this.refresh();
    }));
  }
  
  edit(model: Advert) {
    return this.http.put(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  getByIdAdvert(id: number){
    return this.http.get<Advert>(this.URL+ id);
  }

  getAdvertSkill(id: number){
    return this.http.get<number[]>(this.URL + "getAdvertSkill/" + id);
  }


  addLikeToAdvert(model : AdvertDev) {
    return this.http.post(this.URL+ "AddLikeToAdvert/", model).pipe(finalize(() => { 
      this.refresh();
    }));
  }

  removeLikeFromAdvert(model : AdvertDev) {
    return this.http.post(this.URL+ "RemoveLikeFromAdvert/", model).pipe(finalize(() => {   
      this.refresh();
    }));
  }

  // -------------------------------------------
  AddLikeToDev(model : AdvertDev) {
    return this.http.put(this.URL+ "AddLikeToDev/", model).pipe(finalize(() => { 
      this.refresh();
    }));
  }

  RemoveLikeFromDev(model : AdvertDev) {
    return this.http.put(this.URL+ "RemoveLikeFromDev/", model).pipe(finalize(() => {   
      this.refresh();
    }));
  }
  // ---------------------------------------------------

  getLikedStatus(idD : number){
    return this.http.get<AdvertDev[]>(this.URL + "getLikedStatus/"+ idD);
  }
  
  GetDevByLikedAdvert(idA: number){
    return this.http.get<Developper[]>(this.URL + "GetDevByLikedAdvert/"+ idA);
  }

  getLikedByAdvertStatus(idD : number){
    return this.http.get<AdvertDev[]>(this.URL + "getLikedByAdvertStatus/"+ idD);
  }

}
