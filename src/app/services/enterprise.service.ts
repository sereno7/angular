import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { BehaviorSubject } from 'rxjs';
import { Skill } from '../models/skill';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { Enterprise } from '../models/enterprise';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {

  context: BehaviorSubject<Enterprise[]>;
  private URL: string = environment.AdopteUnDev_ENTERPRISE;

  constructor(private http: HttpClient) { 
    this.context = new BehaviorSubject<Enterprise[]>(null);
    this.refresh();
  }

  refresh() {
    this.http.get<Enterprise[]>(this.URL).subscribe(data => {
      this.context.next(data);
    });
  }
  add(model : Enterprise) {
    return this.http.post(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  delete(id : number) {
    return this.http.delete(this.URL + id).pipe(finalize(() => {
      this.refresh();
    }));
  }

  getById(id: number){
    return this.http.get<Enterprise>(this.URL + id);
  }


  edit(model: Enterprise) {
    return this.http.put(this.URL, model).pipe(finalize(() => {
      this.refresh();
    }));
  }

  
}