export interface Advert{
    Id: number;
    Name: string;
    Date: Date;
    Description: string;
    IdEnterprise: number;
}