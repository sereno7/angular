import { Advert } from "./advert";

export interface Enterprise{
    Id?: number;
    Name: string;
    Email: string;
    Adresse: string;
    Tel: string;
    Fax: string;
    Tva: string;
    Adverts?: Advert[];
}
