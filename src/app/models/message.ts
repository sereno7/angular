export interface Message{
    Id? : number,
    Name: string;
    Date : Date,
    Subject : string,
    IdDevelopper : number,
    IdAdvert : number
}