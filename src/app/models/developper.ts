import { Advert } from "./advert";

export interface Developper{
    Id?: number;
    Last_Name: string;
    First_Name: string;
    Email: string;
    Adresse: string;
    Tel: string;
    Birth_date: string;
    Adverts?: Advert[];
}
